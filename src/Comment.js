import React, {Component} from 'react';
import Avatar from './Avatar';
import UserInfo from './UserInfo';

class Comment extends Component {
    render() {
        return (
            <div className="Comment">
                {/* <div className="UserInfo"> */}
                    {/* <img className="Avatar" src={this.props.author.avatarUrl} alt={this.props.author.name}/> */}
                    {/* <Avatar user={this.props.author}/> */}
                    {/* <div className="UserInfo-name">
                        {this.props.author.name}
                    </div> */}
                {/* </div> */}

                <UserInfo user={this.props.user} />

                <div className="Comment-text">
                    {this.props.text}
                </div>
                <div className="Comment-date">
                    {/* {formatDate(this.props.date)} */}
                    {this.props.date}
                </div>
            </div>
        )
    }
}

export default Comment;