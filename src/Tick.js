import React, {Component} from 'react';
//import logo from './logo.svg'; 
//import './App.css';

var Timer = React.createClass({
    getInitialState: function () {
        return {
            now: new Date().toLocaleTimeString()
        };
    },
    tick: function () {
        this.setState({
            now: new Date().toLocaleTimeString()
        });
    },
    componentDidMount: function () {
        this.interval = setInterval(this.tick, 1000);
    },
    componentWillUnmount: function () {
        clearInterval(this.interval);
    },
    render: function () {
        return (
            <b>Now is: {this.state.now}</b>
        );
    }
});

class Tick extends Component {
    render() {
        return <Timer/>
    }
}

export default Tick;