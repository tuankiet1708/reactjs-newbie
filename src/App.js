import React, {Component} from 'react';
import logo from './logo.svg';
import Tick from './Tick';
import Welcome from './Welcome';
import Comment from './Comment';
import Clock from './Clock';
import Toggle from './Toggle';
import LoginControl from './LoginControl';
import Mailbox from './Mailbox';
import Page from './Page';
import ListItems from './ListItems';
import NameForm from './NameForm';
import EssayForm from './EssayForm';
import FlavorForm from './FlavorForm';
import Reservation from './Reservation';
import Calculator from './Calculator';
import WelcomeDialog, {SplitPane} from './WelcomeDialog';
import SignUpDialog from './SignUpDialog';
import './App.css';

function formatName(user) {
  return user.firstName + ' ' + user.lastName;
}

const user = {
  firstName: 'Kiet',
  lastName: 'Tran'
};

const element = (
  <h1>
    Hello, {formatName(user)}!
  </h1>
);

const userInfo = {
  name: "Kiệt. Trần Lê Tuấn",
  avatarUrl: "avatar.jpg"
}

const messages = ['React', 'Re: React', 'Re:Re: React'];

class App extends Component {
  render() {
    return (
      <div className="App">
        <div className="App-header">
          <img src={logo} className="App-logo" alt="logo"/>
          <br/>
          <Clock date={new Date()}/> {/* <Tick/> */}
          <h2>Hello {formatName(user)}, welcome to React!</h2>
        </div>
        <p className="App-intro">
          To get started, edit &nbsp;<code>src/App.js</code>&nbsp; and save to reload.
        </p>
        {/* <Welcome name="Kiệt Trần"/>
        <Welcome name="Alibaba"/> */}
        <Comment user={userInfo} text="Hello Comment" date="20170926"/> {/* <Clock date={new Date()}/> */}

        <hr/>
        <h3 className="App-lesson">Handling Events</h3>
        <p>
          <Toggle/>
        </p>

        <hr/>
        <h3 className="App-lesson">Conditional Rendering</h3>
        <LoginControl/>
        <Mailbox unreadMessages={messages}/>
        <Page/>

        <hr/>
        <h3 className="App-lesson">Lists and Keys</h3>
        <ListItems/>

        <hr/>
        <h3 className="App-lesson">Forms</h3>
        <NameForm/>
        <EssayForm/>
        <FlavorForm/>
        <Reservation/>

        <hr/>
        <h3 className="App-lesson">Lifting State Up</h3>
        <Calculator/>

        <hr/>
        <h3 className="App-lesson">Composition vs Inheritance</h3>
        <WelcomeDialog/>
        <SplitPane left={'hello'} right={'vietnam'}/>
        <SignUpDialog/>
      </div>
    );
  }
}

export default App;
