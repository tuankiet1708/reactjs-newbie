import React, {Component} from 'react';

class Clock extends Component {
    constructor(props) {
        super(props); 
        this.state = {date: new Date()};
        this.state.collection_1 = 1;
        this.state.collection_2 = 2;
    }

    componentDidMount() {
        this.timerID = setInterval(
            () => this.tick(),
            1000   
        )   
    }

    componentWillUnmount() {
        clearInterval(this.timerID);
    }

    tick() {
        this.setState((prevState, props) => ({
            date: new Date()
        }))

        this.setState((prevState, props) => ({
            collection_1: prevState.collection_1 + 1
        }))

        this.setState((prevState, props) => ({
            collection_2: prevState.collection_2 + 1
        }))

        // console.log('state', this.state);
    }

    render() {
        return (
            <div>
                It is {this.state.date.toLocaleTimeString()}
            </div>
        )
    }
}

export default Clock;