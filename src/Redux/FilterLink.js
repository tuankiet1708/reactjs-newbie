import React, {Component} from 'react';
import PropTypes from 'prop-types';
import {connect} from 'react-redux';
import {setVisibilityFilter} from './Actions';

const Link = ({
    active, 
    currentFilter, 
    onClick,
    children
}) => {
    if (active) {
        return (
            <span>{children}</span>
        )
    }

    return (
        <a
            href='#'
            onClick={e => {
                e.preventDefault();
                onClick();
            }}
        >
            {children}
        </a>
    )
}

// class FilterLink extends Component {
//     static contextTypes = {
//         store: PropTypes.object
//     };

//     constructor(props, context) {
//         super(props);
//         this.store = context.store;
//     }

//     componentDidMount() {
//         this.unsubscribe = this.store.subscribe(() => 
//             this.forceUpdate()
//         );
        
//     }

//     componentWillUnmount() {
//         this.unsubscribe();
//     }

//     render() {
//         const props = this.props;
//         const state = this.store.getState();

//         return (
//             <Link 
//                 active={
//                     props.filter ===
//                     state.visibilityFilter
//                 }
//                 onClick={() => 
//                     this.store.dispatch({
//                         type: 'SET_VISIBILITY_FILTER',
//                         filter: props.filter
//                     })
//                 }
//             >
//                 {props.children}
//             </Link>
//         )
//     }
// }

const mapStateToLinkProps = (
    state, 
    ownProps
) => {
    return {
        active: ownProps.filter === state.visibilityFilter
    }
}

const mapDispatchToLinkProps = (
    dispatch,
    ownProps
) => {
    return {
        onClick: () => dispatch(setVisibilityFilter(ownProps.filter))
    }
}

const FilterLink = connect(mapStateToLinkProps, mapDispatchToLinkProps)(Link);

export default FilterLink;
