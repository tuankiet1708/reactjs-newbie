import React, {Component} from 'react';
import Todo from './Todo';

class TodoList extends Component {
    render() {
        return (
            <ul>
                {this.props.todos.map(t => {
                    console.log(t.id)
                    return (
                        <Todo
                            key={t.id}
                            {...t}
                            onClick={() => this.props.onTodoClick(t.id)}
                        />
                    )
                })}
            </ul>
        )
    }
}

export default TodoList;