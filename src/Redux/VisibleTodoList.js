import React, {Component} from 'react';
import PropTypes from 'prop-types';
import TodoList from './TodoList';
import {connect} from 'react-redux';
import {toggleTodo} from './Actions';

const getVisibleTodos = (todos, filter) => {
    switch (filter) {
        case 'SHOW_ALL':
            return todos;
        case 'SHOW_COMPLETED':
            return todos.filter(t => t.completed);
        case 'SHOW_ACTIVE':
            return todos.filter(t => !t.completed);
        default: 
            return todos;
    }
}

// class VisibleTodoList extends Component {
//     static contextTypes = {
//         store: PropTypes.object
//     };

//     constructor(props, context) {
//         super(props);
//         this.store = context.store;
//     }

//     componentDidMount() {
//         this.unsubscribe = this.store.subscribe(() => 
//             this.forceUpdate()
//         );
        
//     }

//     componentWillUnmount() {
//         this.unsubscribe();
//     }

//     render() {
//         const props = this.props;
//         const state = this.store.getState();

//         return (
//             <TodoList
//                 todos={
//                     getVisibleTodos(
//                         state.todos,
//                         state.visibilityFilter
//                     )
//                 }
//                 onTodoClick={id => 
//                     this.store.dispatch({
//                         type: 'TOGGLE_TODO',
//                         id
//                     })
//                 }
//             />
//         )
//     }
// }


const mapStateToTodoListProps = (state) => {
    return {
        todos: getVisibleTodos(
            state.todos,
            state.visibilityFilter
        )
    }
}

const mapDispatchToTodoListProps = (dispatch) => {
    return {
        onTodoClick: (id) => { 
            dispatch(toggleTodo(id))
        }
    }
}

const VisibleTodoList = connect(
    mapStateToTodoListProps,
    mapDispatchToTodoListProps
)(TodoList);

export default VisibleTodoList;