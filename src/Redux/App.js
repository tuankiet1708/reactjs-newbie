import React, {Component} from 'react';
import PropTypes from 'prop-types';
import TodoApp from './TodoApp';
import {createStore} from 'redux';
import CombineReducers from './CombineReducers';
import {Provider} from 'react-redux';

const store = createStore(CombineReducers);

// class Provider extends Component {
//     static childContextTypes = {
//         store: PropTypes.object
//     };

//     getChildContext() {
//         return {
//             store: this.props.store
//         }
//     }

//     render() {
//         return this.props.children;
//     }
// }

class App extends Component {
    componentDidMount() {
        this.unsubscribe = store.subscribe(() => 
            this.forceUpdate()
        );
    }

    componentWillUnmount() {
        this.unsubscribe();
    }

    render() {  
        return (
            <Provider store={store}>
                <TodoApp/>
            </Provider>
        )
    }
}

export default App;
