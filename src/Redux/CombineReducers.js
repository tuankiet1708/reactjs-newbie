import {combineReducers} from 'redux';
import todos from './Reducers/todos';
import visibilityFilter from './Reducers/visibilityFilter';

const CombineReducers = combineReducers({
    visibilityFilter, 
    todos
});

export default CombineReducers;