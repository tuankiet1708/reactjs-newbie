import React, {Component} from 'react';
import PropTypes from 'prop-types';
import {connect} from 'react-redux';
import {addTodo} from './Actions';

// class AddTodo extends Component {
//     static contextTypes = {
//         store: PropTypes.object
//     };

//     constructor(props, context) {
//         super(props);
//         this.store = context.store;
//     }

//     render() {
//         let input;

//         return (
//             <div>
//                 <input
//                     ref={node => {
//                     input = node;
//                 }}/>

//                 <button
//                     onClick={() => {
//                         this.store.dispatch({
//                             type: 'ADD_TODO',
//                             text: input.value,
//                             id: nextTodoId++
//                         });
//                         input.value = '';
//                     }}
//                 >
//                     Add Todo
//                 </button>
//             </div>
//         )
//     }
// }

let AddTodo = ({dispatch}) => {
    let input;
    
    return (
        <div>
            <input
                ref={node => {
                input = node;
            }}/>

            <button
                onClick={() => {
                    dispatch(addTodo(input.value));
                    input.value = '';
                }}
            >
                Add Todo
            </button>
        </div>
    );
}

// AddTodo = connect(
//     state => {
//         return {}
//     },
//     dispatch => {
//         return { dispatch };
//     }
// );

AddTodo = connect()(AddTodo);

export default AddTodo;