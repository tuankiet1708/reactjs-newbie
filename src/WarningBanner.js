import React, {Component} from 'react';

function WarningBanner(props) {
    if (!props.warn) {
        return null;
    }

    return (
        <div style={ {color: 'red'} }>
            Warning!
        </div>
    );
}

export default WarningBanner;