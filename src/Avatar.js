import React, {Component} from 'react';

class Avatar extends Component {
    render() {
        return (
            <img className="Avatar"
                src={this.props.user.avatarUrl}
                alt={this.props.user.name}
                width="48"
                height="48"
            />
        )
    }
}

export default Avatar;