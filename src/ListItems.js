import React, {Component} from 'react';

class ListItem extends Component {
    render() {
        return (
            <li>{this.props.value}</li>
        )
    }
}

class ListItems extends Component {
    render() {
        const numbers = [1, 2, 3, 4, 5];

        const listItems = numbers.map((number, index) => 
            <ListItem value={number} key={index} /> 
        );

        return <ul>{listItems}</ul>
    }
}

export default ListItems